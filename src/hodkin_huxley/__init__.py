"""Initializing script."""


def hello() -> str:
    """Dummy function."""
    return "Hello from hodkin-huxley!"
